import { createVisit } from "./visit.js";

class Visit {
    constructor(id, fullname, doctor, purpose, description, urgency, completed) {
        this.id = id;
        this.fullname = fullname;
        this.doctor = doctor;
        this.purpose = purpose;
        this.description = description;
        this.urgency = urgency;
        this.completed = completed;
    }

    renderCard() {
        const cards = document.querySelector('.cards');
        const newCard = document.createElement('div');
        newCard.classList.add('new-card');
        cards.append(newCard);
        newCard.innerText = `${this.fullname}, ${this.purpose}, ${this.description}`;
        
    }
}

class VisitDentist extends Visit {
    constructor (fullname, doctor, purpose, description, urgency, completed, lastvisit) {
        super(fullname, doctor, purpose, description, urgency, completed);
        this.lastvisit = lastvisit;
    }
}

class VisitCardiologist extends Visit {
    constructor (fullname, doctor, purpose, description, urgency, completed, pressure, age, weight, illness) {
        super (fullname, doctor, purpose, description, urgency, completed);
        this.pressure = pressure;
        this.age = age;
        this.weight = weight;
        this.illness = illness;
    }
}

class VisitTherapist extends Visit {
    constructor (fullname, doctor, purpose, description, urgency, completed, age) {
        super (fullname, doctor, purpose, description, urgency, completed);
        this.age = age;
    }
}


const visit = new Visit();

export { visit };