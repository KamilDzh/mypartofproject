
import { createVisit } from "./visit.js";
import { visit } from "./cards.js";


class Modal {

    addModalError() {
        const modalError = document.createElement('div');
        modalError.innerText = "Помилка авторизації!";
        modalError.classList.add('modal-error');
        document.body.appendChild(modalError);                
        this.modalWindow.remove();
        const closeErr = document.createElement('div');
        closeErr.innerHTML = '<i class="fa-solid fa-xmark err-delete"></i>';
        modalError.appendChild(closeErr);
        const errDelete = document.querySelector('.err-delete');
        errDelete.addEventListener('click', (e) => {
        modalError.remove();
        });
    }  

    deleteModalError() {
        const windowError = document.querySelector('.modal-error');
        if (document.body.contains(windowError)) {
            windowError.remove();
        }
    }

    deleteFirstInfo() {
        const firstVisit = document.querySelector('.first-vizit');
        if (firstVisit) {
            firstVisit.remove();
        }
    }

    renderModal() {
        const btnExit = document.querySelector('.header-btn');
        btnExit.classList.add('.btn-exit');

        this.modalWindow = document.createElement('div');
        this.modalWindow.innerText = 'Авторизуйтесь, будь ласка';
        this.modalWindow.classList.add('modal');

        const closeModal = document.createElement('div');
        closeModal.innerHTML = '<i class="fa-solid fa-xmark"></i>';
        closeModal.classList.add('btn-delete');

        const inputEmail = document.createElement('div');
        inputEmail.innerHTML = '<label class="input-label">Введіть e-mail<input class="input-modal input-email" type = "text" placeholder="e-mail" /></label>'

        const inputPassword = document.createElement('div');
        inputPassword.innerHTML = '<label class="input-label">Введіть пароль<input  class="input-modal input-pass" type = "password" placeholder="password" /></label>'
        const eyeSlash = document.createElement('div');
        eyeSlash.innerHTML = '<i class="fa-solid fa-eye-slash pass-icon"></i>';
        inputPassword.append(eyeSlash);
        eyeSlash.addEventListener('click', (e) => {
            const inputPass = document.querySelector('.input-pass');
            if (inputPass.getAttribute('type') === 'password') {
                eyeSlash.innerHTML = '<i class="fa-solid fa-eye pass-icon"></i>';
                inputPass.setAttribute('type', 'text');
            } else {
                inputPass.setAttribute(`type`, 'password');
                eyeSlash.innerHTML = '<i class="fa-solid fa-eye-slash pass-icon"></i>';
            };              
        });

        const btnModal = document.createElement('button');
        btnModal.innerText = 'Надіслати';
        btnModal.classList.add('btn-modal');
        btnModal.addEventListener('click', (e) => {
            const valEmail = document.querySelector('.input-email').value;
            const valPass = document.querySelector('.input-pass').value;
            console.log(valEmail, valPass);
            if ((valEmail === "") || (valPass === "")) {
                this.addModalError();
                return
            }
            const email = valEmail;
            const password = valPass;
            (async() => {
                const response = await fetch('https://ajax.test-danit.com/api/v2/cards/login', {
                    method: 'POST',
                    headers: {
                      'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({ email: email, password: password }),
                  });
                  try {
                   const data = await response.text()
                        console.log(data);
                        localStorage.setItem('token', data);
                        if (!(localStorage.getItem('token') == 'Incorrect username or password')) {
                            const btnCreate = document.querySelector('.header-btn');
                            btnCreate.innerText = 'Створити візит';
                            btnCreate.style.fontSize = '16px';
                            this.modalWindow.remove();
                            btnCreate.classList.add('create-vizit');
                            btnCreate.classList.remove('btn-exit');
                            const btnCreateVizit = document.querySelector('.create-vizit');
                            btnCreateVizit.addEventListener('click', (e) => {
                            console.log('Створити візит');
                            this.modalWindow.remove();
                            createVisit.renderModalVisit();
                            });
                        } else {
                            this.addModalError();
                        }
                    } catch {
                            console.log('виникла непередбачувана помилка');
                            this.addModalError();
                    }; 
                })();

                // fetch('https://ajax.test-danit.com/api/v2/cards', {
                //     headers: {
                //         Authorization: `Bearer ${localStorage.getItem('token')}`,
                //     },
                //     })
                //     .then((response) => response.json())
                //     .then((data) => {
                //         console.log(data);
                //         const cardsAll = data;
                //         cardsAll.forEach(card => {
                //             const cards = document.querySelector('.cards');
                //             const newCard = document.createElement('div');
                //             newCard.classList.add('new-card');
                //             cards.append(newCard);
                //             newCard.innerText = `${card.id}, ${card.fullname}, ${card.purpose}`;
                //             this.deleteFirstInfo();
                //         })
                //     });    
            });                 
    
        document.body.append(this.modalWindow);
        this.modalWindow.appendChild(closeModal);
        this.modalWindow.appendChild(inputEmail); 
        this.modalWindow.appendChild(inputPassword);
        this.modalWindow.appendChild(btnModal);
    }

    deleteModal() {
        const btnDelete = document.querySelector('.btn-delete');
        const modalWindow = document.querySelector('.modal');
        btnDelete.addEventListener('click', (e) => {
            modalWindow.remove();
        });
    }
}

const modal = new Modal();
const headerBtn = document.querySelector('.header-btn');
headerBtn.classList.add('btn-exit');
const btnExit = document.querySelector('.btn-exit');
btnExit.addEventListener('click', (e) => {
    modal.deleteModalError();
    modal.renderModal();
    modal.deleteModal();
});

export { modal }