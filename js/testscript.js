

const cardsContainer = document.querySelector('.cards');
console.log(cardsContainer);
function getCards() {
  return fetch('https://ajax.test-danit.com/api/v2/cards', {
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`,
    },
  })
    .then((r) => r.json())
}

const cardsEl = await getCards();
console.log(cardsEl);



function createCardEl() {
  if (cardsEl.length) {
    cardsEl.forEach((visit) => {
      const card = document.createElement('div');
      card.classList.add('card');

      const cardHeader = document.createElement('div');
      cardHeader.classList.add('card-header');

      const name = document.createElement('p');
      name.innerHTML = `ФИО: ${visit.fullname}`;
      cardHeader.appendChild(name);

      const doctor = document.createElement('p');
      doctor.innerHTML = `Врач: ${visit.doctor}`;
      cardHeader.appendChild(doctor);

      const showMore = document.createElement('button');
      showMore.classList.add('show-btn')
      showMore.innerHTML = 'Показать больше';
      cardHeader.appendChild(showMore);

      const edit = document.createElement('button');
      edit.innerHTML = 'Редактировать';
      edit.classList.add('edit-btn');
      cardHeader.appendChild(edit);

      const remove = document.createElement('button');
      remove.classList.add("delete-btn")
      remove.innerHTML = 'х';
      cardHeader.appendChild(remove);

      const cardBody = document.createElement('div');
      cardBody.classList.add('card-body');

      const purpose = document.createElement('p');
      purpose.innerHTML = `Цель визита: ${visit.purpose}`;
      cardBody.appendChild(purpose);

      const description = document.createElement('p');
      description.innerHTML = `Краткое описание: ${visit.description}`;
      cardBody.appendChild(description);

      const urgency = document.createElement('p');
      urgency.innerHTML = `Срочность: ${visit.urgency}`;
      cardBody.appendChild(urgency);

      if (visit.lastvisit) {
        const lastvisit = document.createElement('p');
        lastvisit.innerHTML = `Дата последнего визита: ${visit.lastvisit}`;
        cardBody.appendChild(lastvisit);
      }

      if (visit.age) {
        const age = document.createElement('p');
        age.innerHTML = `Возраст: ${visit.age}`;
        cardBody.appendChild(age);
      }

      if (visit.illness) {
        const illness = document.createElement('p');
        illness.innerHTML = `Болезнь: ${visit.illness}`;
        cardBody.appendChild(illness);
      }

      if (visit.pressure) {
        const pressure = document.createElement('p');
        pressure.innerHTML = `Болезнь: ${visit.pressure}`;
        cardBody.appendChild(pressure);
      }

      if (visit.weight) {
        const weight = document.createElement('p');
        weight.innerHTML = `Вес: ${visit.weight}`;
        cardBody.appendChild(weight);
      }

      const status = document.createElement('p');
      status.innerHTML = `Статус: ${visit.completed}`;
      cardBody.appendChild(status);

      showMore.addEventListener('click', () => {
        if (card.classList.contains('open')) {
          showMore.innerHTML = 'Показать больше';
          card.classList.remove('open');
        } else {
          showMore.innerHTML = 'Показать меньше';
          card.classList.add('open');
        }
      })

      remove.addEventListener('click', () => {
        fetch(`https://ajax.test-danit.com/api/v2/cards/${visit.id}`, {
          method: 'DELETE',
          headers: {
            'Authorization': `Bearer ${localStorage.getItem('token')}`
          },
        })
        card.remove();
      })

      card.appendChild(cardHeader);
      card.appendChild(cardBody);

      cardsContainer.appendChild(card);
    });
  }
}
createCardEl()

const StatusEl = document.querySelector('#status');
const UrgencyEl = document.querySelector('#urgency')
const InputFilter = document.querySelector('#search-title')
let statusValue
let urgencyValue
let textValue

const filteredStatusArr = []

function addEventListeners() {
  StatusEl.addEventListener('change', statusFilter)
  UrgencyEl.addEventListener('change', urgencyFilter)
  InputFilter.addEventListener('input', FilterBytitle) 
}


function statusFilter(e) {
  statusValue = e.target.value;
  cardsEl
    .filter((card) => {
      if (card.completed === statusValue || statusValue === "all" || card.status === "all" ) {
        
        cardsContainer.innerHTML = '';
        console.log(card);
        return card;
      } else {
        cardsContainer.innerHTML = '';
      }

    })
    .forEach((cardsEl) => { 
      const card = new createCardEl(cardsEl);
      
    });
}


function urgencyFilter(e) {
  urgencyValue = e.target.value
  console.log(urgencyValue);
  cardsEl
  .filter((card) => {
  if (card.urgency === urgencyValue || urgencyValue === "all" || card.urgency === "all" )  {
      cardsContainer.innerHTML = '';
      console.log(card);
      return card;
    } else {
      cardsContainer.innerHTML = '';
    }

  })
  .forEach((cardsEl) => {
    const card = new createCardEl(cardsEl);
    console.log(card);
    ;
  });
  console.log(e);
}

function FilterBytitle(e) {
  textValue = e.target.value.toLowerCase();
 cardsContainer.innerHTML = '';

  const filteredInput = cardsEl.filter((card) => {
    for (let value of Object.values(card)) {
      if (String(value).toLowerCase().includes(textValue)) {
        return true;
      }
    }
  });
  filteredInput.forEach((card) => {
    console.log(card);
   return card
  
  })
}



addEventListeners()
